export class TemplateProvider {
    static prefix: string = 'build';
    static componentsUrl: string = `${TemplateProvider.prefix}/components`;
    static pagesUrl: string = `${TemplateProvider.prefix}/pages`;

    constructor() {}

    static getComponentTemplate(componentName: string) {
        return `${TemplateProvider.componentsUrl}/${componentName}/${componentName}.component.html`;
    }

    static getPageTemplate(pageName: string) {
        return `${TemplateProvider.pagesUrl}/${pageName}/${pageName}.page.html`;
    }
}