import {Injectable} from "angular2/core";
import {BaseRequestOptions} from "angular2/http";
import {Response} from "angular2/http";

import {Observable} from "rxjs/Rx";
import {BusStopModel} from "../models/busStop.model";

import {Config} from "../app.config";

import {Loading} from "ionic-angular/index";
import {Parser} from "xml2js";
import {DepartureModel} from "../models/departure.model";
import {RequestOptions} from "angular2/http";
import {Headers} from "angular2/http";
import {Http} from "angular2/http";


@Injectable()
export class BusStopService {
    private httpService: Http;
    private xmlParser: Parser;

    constructor(httpService: Http) {
        this.xmlParser = new Parser();
        this.httpService = httpService;
    }

    public getTimetableObservable(busStopId) {
        let xmlParserObservable$ = Observable.bindCallback(this.xmlParser.parseString);

        return this.httpService.get(Config.NEXT_DEPARTURES_URL + busStopId)
            // -> Observable<Response>
            .map((x: Response) => x.text())
            // -> Observable<Text>
            .flatMap(x => xmlParserObservable$(x))
            // -> Observable<Observable<XmlParsed>> -> Observable<XmlParsed[]>
            .map(x => {
                var timetableData = x[1].Schedules.Stop[0].Day[0].R;
                return timetableData
                    .map((y: any) => {
                        let direction = y.$.dir;
                        let nr = y.$.nr;
                        let time = y.S[0].$.t;
                        let isRealTime =  y.S[0].$.veh !== 'N';
                        let hasVendingMachine: boolean = y.$.vuw.trim() === 'B';
                        return new DepartureModel(time, direction, nr, isRealTime, hasVendingMachine);
                    });
            });
    }

    public getBusStopListObservable() {
        return this.httpService.get(Config.BUSSTOP_LIST_URL)
            // -> Observable<Observable[]>
            .map((res: Response) => res.json())
            // -> Observable<Observable[]>
            .map(observable => {
                return observable.map(x => new BusStopModel(x[0], x[1], x[2], x[4], x[5]));
            });
    }
}