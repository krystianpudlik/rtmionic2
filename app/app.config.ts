import {Injectable} from "angular2/core";

@Injectable()
export class Config {
    static CORS_PROXY =  `https://crossorigin.me/`;
    //static BUSSTOP_LIST_URL = Config.CORS_PROXY + `http://einfo.erzeszow.pl/Home/GetMapBusStopList`;
    static BUSSTOP_LIST_URL = Config.CORS_PROXY + `http://beta.json-generator.com/api/json/get/Ey90-UdW-`;
    static NEXT_DEPARTURES_URL = Config.CORS_PROXY + `http://einfo.erzeszow.pl/Home/GetTimeTableReal?busStopId=`;
    static UPDATE_INTERVAL = 5; // [s]
}