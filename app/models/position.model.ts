export class Position {
    public lat: number;
    public lng: number;

    constructor(lat, lng){
        this.lat = lat;
        this.lng = lng;
    }
}