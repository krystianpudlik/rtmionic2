import {Position} from "./position.model";

export class BusStopModel {
    public id: number;
    public name: string;
    public number: string;
    public position: Position;

    constructor(id: number, name: string, number: string, lat: number, lng: number) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.position = new Position(lat, lng);
    }
}