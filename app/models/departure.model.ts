export class DepartureModel {
    public time: string;
    public destination: string;
    public busNumber: string;
    public isRealTime: boolean;
    public hasVendingMachine: boolean;

    constructor(time: string, destination: string, busNumber: string, isRealTime: boolean, hasVendingMachine: boolean) {
        this.time = time;
        this.destination = destination;
        this.busNumber = busNumber;
        this.isRealTime = isRealTime;
        this.hasVendingMachine = hasVendingMachine;
    }
}