import {Page, NavController, NavParams} from 'ionic-angular';
import {Loading} from "ionic-angular/index";
import {NgClass} from "angular2/common";
import {OnInit} from "angular2/core";

import {TemplateProvider} from "../../services/templateProvider.service";
import {BusStopService} from "../../services/busStop.service";
import {BusStopModel} from "../../models/busStop.model";
import {DepartureModel} from "../../models/departure.model";

@Page({
    templateUrl: TemplateProvider.getPageTemplate('departures'),
    providers: [BusStopService],
    directives: [NgClass]
})
export class DeparturesPage implements OnInit {
    private selectedItem: BusStopModel;
    private timetable: DepartureModel[];
    private loadingIndicator: Loading;

    constructor(private nav:NavController, navParams:NavParams, private busStopService:BusStopService) {
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        this.loadingIndicator = Loading.create({
            content: 'Pobieranie rozkładu'
        });
    }

    ngOnInit() {
        this.nav.present(this.loadingIndicator);

        this.busStopService.getTimetableObservable(this.selectedItem.id)
            .subscribe(next => {
                this.timetable = next;
                this.loadingIndicator.dismiss();
            }, err => console.log(err));
    }

    isDepartureImminent(time: string) {
        return time ? time.indexOf('min') > -1 : false;
    }

    isOneMinuteLeft(time: string) {
        return time === '1 min' || time === '2 min' || time === '<1min';
    }

    getItemClass(item): any {
        return {
            'Departures__time--realTime': this.isDepartureImminent(item.time),
            'Departures__time--oneMinuteLeft': this.isOneMinuteLeft(item.time)
        }
    }
}
