import { Page } from 'ionic-angular';
import { NavParams, NavController, Loading } from "ionic-angular/index";
import { OnInit } from "angular2/core";

import { BusStopService } from "../../services/busStop.service";
import { BusStopModel } from "../../models/busStop.model";
import { TemplateProvider } from "../../services/templateProvider.service";
import { DeparturesPage } from "../departures/departures.page.ts";

@Page({
    templateUrl: TemplateProvider.getPageTemplate('busstops'),
    providers: [BusStopService],
    directives: []
})
export class BusStopsPage implements OnInit {
    private busStopList: BusStopModel[];
    private busStopListBackup: BusStopModel[];
    private searchQuery: string;
    private selectedItem: any;
    private loadingIndicator: Loading;

    constructor(private busStopService:BusStopService, private nav: NavController, navParams: NavParams) {
        this.selectedItem = navParams.get('item');
        this.searchQuery = '';
        this.loadingIndicator = Loading.create({
            content: 'Pobieranie przystanków'
        });
    }

    ngOnInit() {
        this.nav.present(this.loadingIndicator);
        this.busStopService.getBusStopListObservable()
            .subscribe(next => {
                this.busStopList = next;
                this.busStopListBackup = next;
                this.loadingIndicator.dismiss();
            }, err => console.log(err));
    }

    onBusStopTap(event, item) {
        this.nav.push(DeparturesPage, {
            item: item
        });
    }

    filterBusStops(searchbar) {
        // Reset
       this.busStopList = this.busStopListBackup;
        // Get search value
        let searchValue = searchbar.value;
        // Prevent filtering if empty value
        if (searchValue.trim() == '') {
            return;
        }

        this.busStopList = this.busStopList.filter((v) => v.name.toLowerCase().indexOf(searchValue.toLowerCase()) > -1)
    }
}
